module.exports = function(app, jwt, bcrypt, User, keyCookie, formidable, cloudinary, Status){

app.get('/beranda', isLoggedIn, function(req, res){
    User.findAll({ include:[{model:Status}] }).then(ocehan => {
    res.render('beranda', {
        title:'Welcome!!!',
        logged:false,
        a:ocehan,
        i:0,
        j:0
    });
    
    });
});

app.get('/keluar', isLoggedIn, function(req, res){
    res.clearCookie("qwerty");
    res.redirect('/');
});

app.get('/edit', isLoggedIn, function(req, res){
    jwt.verify(req.cookies.qwerty, keyCookie, function(err, decoded){
    res.render('edit', {title:'Silahkan Edit Profile', logged:false, id:decoded.dataUser.id});    
    })
});

app.get('/', sedangLogin, function(req, res){
res.render('home', {title:"Selamat Datang, Silahkan Login", err:false, pass:false, logged:true});
});
    
app.get('/register', sedangLogin, function(req, res){
res.render('register', {title: "Silahkan Register", err:false, logged:true});  
});
    
app.get('/profile', isLoggedIn, function(req, res){
    jwt.verify(req.cookies.qwerty, keyCookie, function(err, decoded) { 
        
       User.findOne({ where : {id: decoded.dataUser.id},
            include: [{ model: Status }]
          }).then(users => {

    res.render('profile', {
                 title:`Selamat Datang ${decoded.dataUser.nama}`,
                 users:decoded.dataUser,
                 logged:!true,
                 status: users
                });
          });
    });
});


    
}; //end of module Export

const isLoggedIn = (req, res, next) => {
    if(req.cookies.qwerty) return next();
    res.redirect('/');
};



const sedangLogin = (req, res, next) => {
    if(!req.cookies.qwerty) return next();
    res.redirect('/profile');
};