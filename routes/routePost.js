module.exports = function(app, jwt, bcrypt, User, keyCookie, formidable, cloudinary, Status){
    
    app.post('/profile', function(req, res){
        const isi = req.body.isi,
              id  = parseInt(req.body.id);

              Status.create({
                isi: isi,
                userId: id
              });

              res.send("Berhasil Memasukan Status");
      });
      
      
      app.post('/', function(req, res){
          User.findOne({where: {email: req.body.email}}).then(user => {
              if(user==null){
                  res.render('home', {title:'Selamat Datang Silahkan Login', err:true, pass:false});
              }else{
                  user.gambar = cloudinary.url(user.gambar); 
                  bcrypt.compare(req.body.pass, user.passsword, function(err, hasil) {
                     if(hasil){       
                          const token = jwt.sign({dataUser: user}, keyCookie);
                          res.cookie('qwerty', token);
                          res.redirect('/profile');
                      }else{
                  res.render('home', {title:'Selamat Datang Silahkan Login', err:false, pass:true});
                      }
                  });
              }
          });
      });
      
      app.post('/upload', (req, res) =>{
      
          cloudinary.v2.uploader.upload(req.files.img.path, 
          function(error, result) {
      
              User.findOne({ where: { id: parseInt(req.fields.id) } })
              .then(user => {
      
                if (user) {
      
                  user.updateAttributes({
                    gambar: `${result.public_id}.${result.format}`
                  });
      
                  res.clearCookie("qwerty");
                  user.gambar = cloudinary.url(user.gambar);                 
                  const token = jwt.sign({dataUser: user}, keyCookie);
                  res.cookie('qwerty', token);
                  res.redirect('/profile');
      
                }
              });
             
          });
      });
      
      app.post('/register', function(req, res){
          User.findOne({ where: {email: req.body.email} }).then(user => {
              if(user==null){
      
                  bcrypt.hash(req.body.passsword, null, null, function(err, hash) {
                      req.body.passsword = hash;
                      User.create(req.body);
                      res.redirect('/');
                 });
      
              }else{
                  res.render('register', {title: 'Silahkan Register', err:true})            
              }
      
            });
      
      });
}