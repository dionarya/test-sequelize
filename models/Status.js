const Sequelize = require('sequelize');
const sequelize = require('../config/configSequelize');
const Status = sequelize.define('status', {
    isi: {
      type: Sequelize.STRING
    }
  });

module.exports = Status;