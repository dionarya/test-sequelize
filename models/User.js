const Sequelize = require('sequelize');
const sequelize = require('../config/configSequelize');
const User = sequelize.define('user', {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
    nama: {
      type: Sequelize.STRING
    },
    username: {
      type: Sequelize.STRING
    },
    passsword:{
      type: Sequelize.STRING
    },
    email : {
      type: Sequelize.STRING
    },
    gambar :{
      type: Sequelize.STRING
    }
  });

module.exports = User;