const express      = require('express'),
      bcrypt       = require('bcrypt-nodejs'),
      mysql2       = require('mysql2'),
      ejs          = require('ejs'),
      Sequelize    = require('sequelize'),
      path         = require('path'),
      bodyParser   = require('body-parser'),
      router       = express.Router(),
      cookieParser = require('cookie-parser'),
      jwt          = require('jsonwebtoken'),
      keyCookie    = 'ajksdjkashdjahdkjashd',
      app          = express(),
      formidable   = require('express-formidable'),
      cloudinary   = require('./config/configCloudinary');
      User         = require('./models/User'),
      Status       = require('./models/Status');

app.use("/public", express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(formidable());

//Routes
require('./routes/routeGet')(app, jwt, bcrypt, User, keyCookie, formidable, cloudinary, Status);
require('./routes/routePost')(app, jwt, bcrypt, User, keyCookie, formidable, cloudinary, Status);

//Relasi
Status.belongsTo(User);  
User.hasMany(Status);

app.listen(3000, () => {
    console.log("Magic In http://127.0.0.1:3000");
});